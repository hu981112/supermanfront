const path = require('path');
const resolve = dir => {
  return path.join(__dirname, dir);
};

module.exports = {
  publicPath: '/',
  outputDir: 'dist',
  assetsDir: 'static',
  lintOnSave: process.env.NODE_ENV === 'development',
  productionSourceMap: false,
  devServer: {
    port: '8080',
    open: true,
    overlay: {
      warnings: false,
      errors: true,
    },
    proxy: {
      '/api': {
        target: 'http://101.35.159.198:34567',
        //target: 'http://127.0.0.1:8081',
      },
    },
  },
  // webpack 配置
  // chainWebpack: (config) => {
  //   config.entry.app = ['./src/main.js']
  //   config.resolve.alias
  //     .set('@', resolve('src/*'))
  // },


  configureWebpack: {
    resolve: {
      alias: {
        '@': resolve('src/*')
      }
    }
  }
};
