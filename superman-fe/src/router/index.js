import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/login',
    name: 'login',
    component: () => import(/* webpackChunkName: "login" */ '../views/login/index.vue')
  },
  {
    path: '/',
    name: 'Home',
    component: Home,
    children: [
      {
        path: '/tests',
        name: 'tests',
        component: () => import(/* webpackChunkName: "tests" */ '../views/tests/index.vue'),
      },
      {
        path: '/docs',
        name: 'docs',
        component: () => import(/* webpackChunkName: "tests" */ '../views/docs/index.vue'),
      },
      {
        path: '/envs',
        name: 'envs',
        component: () => import(/* webpackChunkName: "envs" */ '../views/envs/index.vue'),
      },
      {
        path: '/users',
        name: 'users',
        component: () => import(/* webpackChunkName: "users" */ '../views/users/index.vue'),
      },
      {
        path: '/websockets',
        name: 'websockets',
        component: () => import(/* webpackChunkName: "websockets" */ '../views/websockets/index.vue'),
      },
      {
        path: '/batch',
        name: 'batch',
        component: () => import(/* webpackChunkName: "batch" */ '../views/batch/index.vue'),
      },
      {
        path: '/feedback',
        name: 'feedback',
        component: () => import(/* webpackChunkName: "feedback" */ '../views/feedback/index.vue'),
      },
      {
        path: '/feedbacks',
        name: 'feedbacks',
        component: () => import(/* webpackChunkName: "feedbacks" */ '../views/feedback/admin.vue'),
      },
      {
        path:'/loggings',
        name:'loggings',
        component: () => import(/* webpackChunkName: "feedbacks" */ '../views/loggings/index.vue'),
      }
    ]
  },
  {
    path: '/doc',
    name: 'doc',
    component: () => import(/* webpackChunkName: "doc" */ '../views/docs/doc.vue'),
    children: [
      {
        path: '/doclist/:uuid',
        name: 'doclist',
        component: () => import(/* webpackChunkName: "lists" */ '../views/docs/lists.vue'),
      },
      {
        path: '/docdetail',
        name: 'docdetail',
        component: () => import(/* webpackChunkName: "detail" */ '../views/docs/detail.vue'),
      }
    ]
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
