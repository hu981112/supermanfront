import { Message, Notification } from 'element-ui';

const DURATION = 2000;

export function showMessage(
  message,
  type = 'error',
) {
  Message({
    type,
    message: message || '错误',
    duration: DURATION,
  });
}

export function showNotification(
  title,
  message,
  type = 'info',
) {
  Notification({
    title,
    message,
    type,
    duration: DURATION,
  });
}
