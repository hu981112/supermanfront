import router from '../router';
import axios from 'axios';
import { showMessage } from './message';

const instance = axios.create({
  baseURL: process.env.VUE_APP_BASE_API,
  // timeout: 30000,
});

// 请求拦截器
instance.interceptors.request.use(async (config) => {
  console.log("进入请求拦截器")

  const TOKEN = sessionStorage.getItem('TOKEN') || '';

  console.log("config", config);
  

  if (TOKEN) {
    config.headers.auth = TOKEN;
  } else if (config.url !== '/api/auth' && config.url !== '/refresh' && config.url !== '/api/users/register' && !config.url.startsWith("/api/share/doc/") && !config.url.startsWith("/api/share/detail")) {
    await router.replace('/login');
  }
  return config;
}, (error) => {
  console.error(error);
  return Promise.reject(error);
});

// 响应拦截器
instance.interceptors.response.use((res) => {
  console.log("进入响应拦截器")
  const { status, data } = res;
  if (status === 200 && data.status === 200) {
    return Promise.resolve(res);
  }
  if (data.status && data.status !== 200) {
    showMessage(data.message);
  }
  return Promise.reject(res);
}, (error) => {
  console.error(error);
  const { status, data } = error.response;
  showMessage(data.message);
  switch (status) {
    case 401:
      router
        .replace('/login')
        .catch((e) => console.error(e));
      break;
    default:
      break;
  }
  return Promise.reject(error);
});

export default instance;





