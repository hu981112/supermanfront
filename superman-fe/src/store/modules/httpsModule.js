/**
 * @description 接口模块
 * @author jjjjjy
 */

const httpsModule = {
    state: {
        taskname: '',
    },
    getters: {
        taskname: state => state.taskname,
    },
    mutations: {
        SET_TASK_NAME: (state, playload) => {
            state.taskname = playload;
        }
    },
}

export default httpsModule