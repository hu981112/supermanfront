/**
 * @description 响应模块
 * @author jjjjjy
 */
const responsesModule = {
    state: {
        url: '',
        header: '',
        method: '',
        status: '',
        body: '',
        time: '',
        contentType: '',
    },
    getters: {
        url: state => state.url,
        header: state => state.header,
        method: state => state.method,
        status: state => state.status,
        body: state => state.body,
        time: state => state.time,
        contentType: state => state.contentType
    },
    mutations: {
        SET_RESPONSE: (state, playload) => {
            state = playload
        },
        SET_RESPONSE_URL: (state, playload) => {
            state.url = playload;
        },
        SET_RESPONSE_Header: (state, playload) => {
            state.header = playload;
        },
        SET_RESPONSE_Method: (state, playload) => {
            state.method = playload;
        },
        SET_RESPONSE_STATUS: (state, playload) => {
            state.status = playload;
        },
        SET_RESPONSE_BODY: (state, playload) => {
            state.body = playload;
        },
        SET_RESPONSE_TIME: (state, playload) => {
            state.time = playload;
        },
        SET_RESPONSE_TYPE: (state, playload) => {
            state.contentType = playload;
        },


    },
    actions: {

    }
}

export default responsesModule;