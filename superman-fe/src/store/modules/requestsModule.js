/**
 * @description 请求模块
 * @author jjjjjy
 */
const requestsModule = {
    state: {
        url: '',
        header: [],
        method: '',
        query: [],
        body: '',
        type: '',
    },
    getters: {
        url: state => state.url,
        header: state => state.header,
        method: state => state.method,
        query: state => state.query,
        body: state => state.body,
        type: state => state.type
    },
    mutations: {
        SET_REQUESTS_URL: (state, playload) => {
            state.url = playload;
        },
        SET_REQUESTS_Header: (state, playload) => {
            state.header = playload;
        },
        SET_REQUESTS_Method: (state, playload) => {
            state.method = playload;
        },
        SET_REQUESTS_QUERY: (state, playload) => {
            state.query = playload;
        },
        SET_REQUESTS_BODY: (state, playload) => {
            state.body = playload;
        },
        SET_REQUESTS_TYPE: (state, playload) => {
            state.type = playload;
        },


    },
    actions: {

    }
}

export default requestsModule;