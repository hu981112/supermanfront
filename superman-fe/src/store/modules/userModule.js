/**
 * @description 用户模块
 * @author jjjjjy
 */

import { login } from "../../api/login"


const userModule = {
    userId: '',
    mutations: {
        SET_USER_ID(id) {
            this.userId = id;
        }
    },
    actions: {
        // 登出
        Logout() {
            this.userId = 0;
            sessionStorage.setItem('TOKEN', '');
        },
        // 登录
        async Login(username, password) {
            const data = await login(username, password);
            const { token } = data.data.data;
            sessionStorage.setItem('TOKEN', token);
            sessionStorage.setItem('username', username);
        }
    }
}

export default userModule