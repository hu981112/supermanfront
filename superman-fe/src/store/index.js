import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

import userModule from './modules/userModule'
import responsesModule from './modules/responsesModule'
import requestsModule from './modules/requestsModule'
import httpsModule from './modules/httpsModule'

export default new Vuex.Store({
  state: {
  },
  mutations: {
  },
  actions: {
  },
  modules: {
    userModule,
    requestsModule,
    responsesModule,
    httpsModule
  }
})
