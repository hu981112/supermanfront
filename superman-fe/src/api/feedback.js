import request from '../utils/request';


export function FeedBackList(pageNo,pageSize,type) {
    return request({
        method: 'get',
        url: '/api/feedback/',
        params: {
            pageNo:pageNo,
            pageSize:pageSize,
            type:type
        }
    }).then((res) => res.data);
}

export function DeleteFeedBack(id) {
    return request({
        method: 'delete',
        url: '/api/feedback/'+id,
        data: {},
    }).then((res) => res.data);
}


export function AddFeedback(type,text) {
    return request({
        method: 'POST',
        url: '/api/feedback/create',
        data: {
            type:type,
            text:text
        },
    }).then((res) => res.data);
}


/**
 * @description 
 */
