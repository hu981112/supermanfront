import request from '../utils/request';


/**
 * @description 返回用户的所有http任务
 * @author jjjjjy
 * @returns List<HttpTaskPO>
 */
export function getAllHttpTask() {
    return request({
        method: 'GET',
        url: `/api/httptask/all`
    }).then((res) => res.data);
}

/**
 * @description 根据id获取一条http任务
 * @param {} id 
 * @returns HttpTaskPO
 */
export function getHttpTaskById(id) {
    return request({
        method: 'GET',
        url: `/api/httptask/${id}`
    }).then((res) => res.data);
}

/**
 * @description 新增一条http任务
 * @param {*} HttpTaskBO 
 * @returns 
 */
export function createHttpTask(taskname, requests, responses) {
    return request({
        method: 'POST',
        url: '/api/httptask/create/',
        data: {
            taskname: taskname,
            request: requests,
            response: responses
        },
    }).then((res) => res.data);

}

/**
 * @description 修改一条http任务
 * @param {*} HttpTaskPO 
 * @returns 
 */
export function updateHttpTask(HttpTaskPO) {
    return request({
        method: 'POST',
        url: `/api/httptask/update?HttpTaskPO=${HttpTaskPO}`
    })
}

/**
 * @description 删除一条http任务
 * @param {*} id 
 * @returns 
 */
export function deleteHttpTaskById(id) {
    return request({
        method: 'POST',
        url: `/api/httptask/delete/${id}`
    })
}

/**
 * @description 执行一条http请求
 * @param {} HttpReqBO 
 * @returns HttpTaskBO
 */
export function runHttpTask(url, header, method, query, body, type) {
    return request({
        method: 'POST',
        url: '/api/httptask/run/',
        data: {
            url: url,
            header: header,
            method: method,
            query: query,
            body: body,
            type: type
        },
    }).then((res) => res.data);
}
/**
 * @description 下载一条http任务
 * @param {*} id 
 * @returns 
 */
 export function downloadHttpTaskById(id,config) {
    return request({
        method: 'get',
        url: `/api/httptask/download/${id}`,
        config:config
    }).then(res=>{
        console.log("收到数据")
        console.log(res)
        return res
    }).catch(err=>{
        console.log("接收数据错误")
        console.log(err)
        return err
    })
}
/**
 * @description 上传一条http任务
 * @param {*} file
 * @returns 
 */
 export function uploadHttpTask(data) {
    return request({
        method: 'POST',
        url: `/api/httptask/upload`,
        headers:{"Content-Type": "multipart/form-data; charset=UTF-8"},
        data:data
    })
}