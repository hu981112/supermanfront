import request from '../utils/request';


export function BatchList() {
    return request({
        method: 'get',
        url: '/api/batchtest/',
        data: {},
    }).then((res) => res.data);
}

export function DeleteBatch(id) {
    return request({
        method: 'delete',
        url: '/api/batchtest/'+id,
        data: {},
    }).then((res) => res.data);
}


export function AddBatch(name,tasks) {
    return request({
        method: 'POST',
        url: '/api/batchtest/create',
        data: {
            testName:name,
            tasks:tasks
        },
    }).then((res) => res.data);
}


export function RunBatch(id) {
    return request({
        method: 'GET',
        url: '/api/batchtest/'+id,
        data: {
        },
    }).then((res) => res.data);
}
export function DoBatch(id,times,catchable) {
    return request({
        method: 'GET',
        url: '/api/batchtest/do?id='+id+'&catchable='+catchable+'&times='+times,
        data: {
        },
    }).then((res) => res.data);
}

/**
 * @description 
 */
