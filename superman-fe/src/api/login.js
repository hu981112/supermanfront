import request from '../utils/request';

/**
 * @description 登陆固定账号 admin
 * @author jjjjjy
 * @param username 
 * @param password 
 * @returns 
 */
export function login(username, password) {
    return request({
        method: 'POST',
        url: '/api/auth',
        data: { username, password },
    }).then((res) => res.data);
}

/**
 * @description 
 */
