import request from '../utils/request';


export function envlist() {
    return request({
        method: 'get',
        url: '/api/envvar/list',
        data: {},
    }).then((res) => res.data);
}

export function DeleteEnv(id) {
    return request({
        method: 'post',
        url: '/api/envvar/delete/'+id,
        data: {},
    }).then((res) => res.data);
}

export function EditEnv(id, key, val) {
    return request({
        method: 'post',
        url: '/api/envvar/update/',
        data: {
            id: id,
            name:key,
            value:val
        },
    }).then((res) => res.data);
}


export function AddEnv(key,val) {
    return request({
        method: 'POST',
        url: '/api/envvar/create/',
        data: {
            name:key,
            value:val
        },
    }).then((res) => res.data);
}
/**
 * @description 
 */
