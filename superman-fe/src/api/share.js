import request from '../utils/request';


export function ShareList() {
    return request({
        method: 'get',
        url: '/api/share/list',
        data: {},
    }).then((res) => res.data);
}

export function DeleteShare(id) {
    return request({
        method: 'post',
        url: '/api/share/delete/' + id,
        data: {},
    }).then((res) => res.data);
}

export function EditShare(id, name, tasks) {
    return request({
        method: 'post',
        url: '/api/share/update/',
        data: {
            id: id,
            shareName: name,
            taskIds: tasks
        },
    }).then((res) => res.data);
}


export function AddShare(name, tasks) {
    return request({
        method: 'POST',
        url: '/api/share/create',
        data: {
            shareName: name,
            taskIds: tasks
        },
    }).then((res) => res.data);
}



export function DocList(uuid) {
    return request({
        method: 'GET',
        url: '/api/share/doc/' + uuid,
        data: {},
    }).then((res) => res.data);
}



export function DocDetail(uuid, taskid) {
    return request({
        method: 'GET',
        url: '/api/share/detail',
        params: {
            uuid: uuid,
            taskId: taskid
        }
    }).then((res) => res.data);
}


/**
 * @description 分页条件筛选返回文档
 * @param {*} pageNo 
 * @param {*} pageSize 
 * @param {*} shareName 
 * @returns PageModel
 */
export function getDocByPage(pageNo, pageSize, shareName) {
    return request({
        method: 'GET',
        url: `/api/share/listByPage?pageNo=${pageNo}&pageSize=${pageSize}&shareName=${shareName}`
    }).then((res) => res.data);
}

