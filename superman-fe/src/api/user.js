import request from '../utils/request';


export function userlist() {
    return request({
        method: 'get',
        url: '/api/users/',
        data: {},
    }).then((res) => res.data);
}

export function DeleteUser(id) {
    return request({
        method: 'delete',
        url: '/api/users/'+id,
        data: {},
    }).then((res) => res.data);
}

export function EditUser(user) {
    return request({
        method: 'put',
        url: '/api/users/'+user.id,
        data: {
          username:user.username,
          password:user.password,
          enabled:user.enabled,
          phone:user.phone,
          roles:[0],

        },
    }).then((res) => res.data);
}


export function AddUser(key,val,phone) {
    return request({
        method: 'POST',
        url: '/api/users/',
        data: {
            username:key,
            password:val,
            phone:phone,
            enabled:true,
            roles:[1]
        },
    }).then((res) => res.data);
}


export function register(key,val,phone) {
    return request({
        method: 'POST',
        url: '/api/users/register',
        data: {
            username:key,
            password:val,
            phone:phone,
            enabled:true,
            roles:[1]
        },
    }).then((res) => res.data);
}
/**
 * @description 
 */
