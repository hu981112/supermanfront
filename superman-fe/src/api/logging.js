import request from '../utils/request';

/**
 * @description 分页返回请求日志
 * @author jjjjjy
 * @param {*} pageNo 
 * @param {*} pageSize 
 * @returns PageModel
 */
export function getLogging(pageNo, pageSize) {
    return request({
        method: 'GET',
        url: `/api/logs/access/?pageNo=${pageNo}&pageSize=${pageSize}`,
    }).then((res) => res.data);
}

/**
 * @description 删除一条请求日志
 * @author jjjjjy
 * @returns 
 */
export function deleteLogging(id) {
    return request({
        method: 'DELETE',
        url: `/api/logs/access/${id}`,
    })
}


